#!/bin/sh
repo=https://gitlab.com/tinydesktoplinux/xpkg/-/raw/main/packages/
# Buscar la partición sda1 en /proc/mounts
root_partition=$(grep "/mnt" /proc/mounts | cut -d " " -f 1)
# Extraer el nombre de la partición
partition_name=$(basename "$root_partition")
# Comienza
cd /tmp
wget "$repo"ncspot.tcz --no-check-certificate
tce-load -i ncspot.tcz
tce-load -wi libpulseaudio.tcz
mv ncspot.tcz /mnt/"$partition_name"/tce/optional
echo ncspot.tcz >> /mnt/"$partition_name"/tce/onboot.lst
rm ncspot.sh
echo 'ncspot installed.'
exit 0
