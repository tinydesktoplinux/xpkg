#!/bin/sh
repo=https://gitlab.com/tinydesktoplinux/xpkg/-/raw/main/packages/
# Buscar la partición sda1 en /proc/mounts
root_partition=$(grep "/mnt" /proc/mounts | cut -d " " -f 1)
# Extraer el nombre de la partición
partition_name=$(basename "$root_partition")
# Comienza
cd /tmp
wget "$repo"gens.tcz --no-check-certificate
tce-load -i gens.tcz
tce-load -wi SDL.tcz gtk2.tcz
mv gens.tcz /mnt/"$partition_name"/tce/optional
echo gens.tcz >> /mnt/"$partition_name"/tce/onboot.lst
rm gens.sh
echo 'gens installed.'
exit 0
