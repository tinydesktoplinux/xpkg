#!/bin/sh
# Buscar la partición sda1 en /proc/mounts
root_partition=$(grep "/mnt" /proc/mounts | cut -d " " -f 1)
# Extraer el nombre de la partición
partition_name=$(basename "$root_partition")
# Comienza
cd /tmp
wget https://locosporlinux.com/tdl/midori.tcz --no-check-certificate
tce-load -i midori.tcz
tce-load -wi gtk3.tcz dbus-glib.tcz
mv midori.tcz /mnt/"$partition_name"/tce/optional
echo midori.tcz >> /mnt/"$partition_name"/tce/onboot.lst
rm -f midori.sh
echo 'Midori installed.'
exit 0
