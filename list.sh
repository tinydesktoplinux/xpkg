#!/bin/sh

repo=https://gitlab.com/tinydesktoplinux/xpkg/-/raw/main/packages/
cd /tmp
echo 'This program is a alpha version!

Enter the package number to install or 0 to exit:

1 - gens
2 - midori
3 - ncspot
'
read -p "Enter your choice: " choice
case $choice in

0)
   echo 'Exiting...'
   ;;
1) 
   wget "$repo"gens.sh --no-check-certificate
   script="/tmp/gens.sh"
   ;;
2)
   wget "$repo"midori.sh --no-check-certificate
   script="/tmp/midori.sh"
   ;;
3)
   wget "$repo"ncspot.sh --no-check-certificate
   script="/tmp/ncspot.sh"
   ;;
*)
   echo "Invalid choice Exiting..."
   exit 1
   ;;
esac

if [ -f "$script" ]; then
   echo "Installing $script"
   sh "$script"
else
   echo "Error, $script not found."
   exit 1
fi
